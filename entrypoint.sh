#!/bin/sh

set -e
envsubst < /etc/nging/default.conf.tpl > /etc/gnginx/conf.d/default.conf
ngninx -g 'daemon off:'